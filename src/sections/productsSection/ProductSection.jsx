import React,{useEffect,useState} from 'react';
import Product from "../../components/Product";


const ProductsSection = () => {

    const [productList, setProductList] = useState([]);

    useEffect(() => {
        fetch('./products.json')
            .then(response => response.json())
            .then(result => setProductList(result));
    });

    return(
        <section className='products-section'>
            {productList && productList.map((elem, index) => {
                return(
                    <Product
                        key={index}
                        {...elem}
                    />
                )
            })}
        </section>
    );
};


export default ProductsSection;