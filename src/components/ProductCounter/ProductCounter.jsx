import React,{useState} from 'react';
import Button from "../Button";


const ProductCounter = () => {

    const [count, setCount] = useState(1);

    return(
        <div className='product-counter'>
            <Button onClick={() => count > 1 && setCount(count-1)} className='product-counter__button'>-</Button>
            {count}
            <Button onClick={() => setCount(count+1)} className='product-counter__button'>+</Button>
        </div>
    );
};


export default ProductCounter;