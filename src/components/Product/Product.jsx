import React,{useState} from 'react';
import Button from "../Button";
import Checkbox from "../Checkbox";
import ProductCounter from "../ProductCounter";
import PropTypes from "prop-types";


const Product = ({title, description, price, img}) => {

    const [compareSubmit, setCompareSubmit] = useState(false);

    const handleChangeCompare = () => {
        setCompareSubmit(true);
        setTimeout(() => setCompareSubmit(false),1500);
    };

    return(
        <div className='product'>
            <div className='product__top-box'>
                <Button className='product__new'>new</Button>
                <div className='product__img-box'><img src={img} alt={`${title}-img`}/></div>
                <Button className='product__compare' onClick={handleChangeCompare}>
                    {!compareSubmit && <i className="fas fa-balance-scale-right"> </i>}
                    {compareSubmit && <i className="fas fa-check product__compare-check"> </i>}
                </Button>
            </div>
            <div className='product__middle-box'>
                <h2>{title}</h2>
                <p>{description}</p>
            </div>
            <div className='product__color-price'>
                <select className='product__color'>
                    <option>Цвет</option>
                    <option style={{height: '200px'}} value="Желтый">Желтый</option>
                    <option value="Красный">Красный</option>
                    <option value="Зеленый">Зеленый</option>
                    <option value="Синий">Синий</option>
                    <option value="Розовый">Розовый</option>
                    <option value="Оранжевый">Оранжевый</option>
                </select>
                <p className='product__price'>{price} грн</p>
            </div>
            <Checkbox />
            <div className='product__bottom-box'>
                <ProductCounter />
                <Button className='product__buy'>купить</Button>
            </div>
        </div>
    );
};

Product.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.string.isRequired,
    img: PropTypes.string,
};

Product.defaultProps = {
    title: 'Undefined product',
    description: 'None',
    price: '200',
    img: '',
};

export default Product;