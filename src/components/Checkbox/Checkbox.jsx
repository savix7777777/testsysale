import React,{useState} from 'react';


const Checkbox = () => {

    const [checkState, setCheckState] = useState({
        first: true,
        second: false,
        third: false,
    })

    const changeSize = ({target}) => {
        const newCheckState = Object.assign({},checkState);
        newCheckState.first = false;
        newCheckState.second = false;
        newCheckState.third = false;
        newCheckState[target.id] = !newCheckState[target.id];
        setCheckState(newCheckState);
    };

    return (
        <div className='checkbox'>
            <div className='checkbox__sub-box'>
                <input onChange={changeSize} id='first' checked={checkState.first && true} className="check" type="checkbox" name="check" value="check" />
                <p>100 мл</p>
            </div>
            <div className='checkbox__sub-box'>
                <input onChange={changeSize} checked={checkState.second && true} className="check" id='second' type="checkbox" name="check" value="check" />
                <p>200 мл</p>
            </div>
            <div className='checkbox__sub-box'>
                <input onChange={changeSize} checked={checkState.third && true} className="check" id='third' type="checkbox" name="check" value="check" />
                <p>300 мл</p>
            </div>
        </div>
    );
};


export default Checkbox;