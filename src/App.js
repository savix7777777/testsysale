import React from 'react';
import './styles/index.scss'
import ProductsSection from "./sections/productsSection";

function App() {
  return (
    <div className="app">
        <ProductsSection />
    </div>
  );
}

export default App;
